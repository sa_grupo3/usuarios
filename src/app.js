'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const logger = require("morgan");
const Usuario = require('../models/usuario')
const Contador = require('../models/contador')

const app = express()

const jwt = require('jsonwebtoken')
const fs = require ('fs')
const connect = mongoose.connect('mongodb://18.206.38.147:27017/usuario', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
var publicKey=process.env.PUBLIC_KEY||"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAQA5vqqDW0VcY791GwD5NzII5DPO9cWQfDGu6G/nUUkOFbef1KP9t7MCbqE/fpry5bBnkeaxWp4VzC2Nck0DQ4IuWUC3Uhz9VewCheYgCY/pSGaJoMYJyLk6Bjh98knJ1GtylHsJ+N8JlnVg4FLqyYSFdbV5YGz82iREAYseRMwIDAQAB\n-----END PUBLIC KEY-----"

connect.then(() => {
  //console.log("Connected correctly to server");
}, (err) => { console.log(err); });


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(logger("dev"));
app.get('/',(req, res) => {
    res.send({ message: "Microservicio de usuarios" })
})

app.get('/login', (req, res) => {

     const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token, publicKey, verifyOptions, function (err, verifiacion) {
        if (err) {
            res.status(400).send({ message: "Token invalido" })
        } else {
    
            console.log(req.body)
    
            let usuario = req.query.email
            let password = req.query.password

            Usuario.findOne({ email: usuario, password: password }, (err, usuarioDB) => {
                if (err) res.status(500).send({ message: `Error al loguearse: ${err}` })
                if (!usuarioDB) res.status(404).send({ message: `El usuario o password no coinciden` })
                res.status(200).send({ usuario: usuarioDB })
            })
        }
    })
})



app.get('/jugadores', (req, res) => {

     const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token,publicKey,verifyOptions,function(err, verifiacion){
        if(err){
            res.status(400).send({message: "Token invalido"})
        }else{
            Usuario.find({}, (err, usuario) => {
        if (err) res.status(500).send({message: `Error al realizar la peticion: ${err}`})
        if (!usuario) res.status(404).send({message: `Usuarios inexistentes: ${err}`})
        res.status(200).send({usuario})
        })
        }
    })  
})

app.get('/jugadores/:id', (req, res) => {
    
     const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token, publicKey, verifyOptions, function (err, verifiacion) {
        if (err) {
            res.status(400).send({ message: "Token invalido" })
        } else {
            let usuarioId = req.params.id

            Usuario.findOne({ id: usuarioId }, (err, usuario) => {
                if (err) res.status(500).send({ message: `Error al realizar la peticion: ${err}` })//500 Datos invalidos
                if (!usuario) res.status(404).send({ message: `Usuario no encontrado: ${err}` })
                res.status(200).send({ usuario })
            })
        }
    })
})

app.post('/jugadores', async function (req, res) {
    
     const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token, publicKey, verifyOptions,async function (err, verifiacion) {
        if (err) {
            res.status(400).send({ message: "Token invalido" })
        } else {
            let contador = await Contador.find()
            let id = contador.length

            let contador2 = new Contador()
            contador2.contador = id
    
            contador2.save((err, contadorStore) => {
                if (err) res.status(401).send({ message: `Error al guardar generador: ${err}` })//500 Datos invalidos
            })

            let usuario = new Usuario()
            usuario.id = id
            usuario.nombres = req.body.nombres
            usuario.apellidos = req.body.apellidos
            usuario.email = req.body.email
            usuario.password = req.body.password
            usuario.administrador = req.body.administrador

            usuario.save((err, usuarioStore) => {
                if (err) res.status(401).send({ message: `Error al guardar nuevo usuario: ${err}` })//500 Datos invalidos
                res.status(201).send({ usuario: usuarioStore })
            })
        }
    })
})

app.put('/jugadores/:id', (req, res) => {
    
     const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token, publicKey, verifyOptions, function (err, verifiacion) {
        if (err) {
            res.status(400).send({ message: "Token invalido" })
        } else {
            let jugadorId = req.params.id
            let update = req.body
            
            Usuario.findOneAndUpdate({ id: jugadorId }, {
                $set: req.body
            }, { new: true })
                .then((usuarioUpdate) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.status(201).send({ usuario: usuarioUpdate })
                })
                .catch((err) => {
                    res.status(401).send({ message: `Error al guardar nuevo usuario: ${err}` })//500 Datos invalidos 
                });
        }
    })
})

app.delete('/jugadores/:id', (req, res) => {
    let jugadorId = req.params.id

    Usuario.findOne({id: jugadorId}, (err, usuario) => {
        if (err) res.status(500).send({message: `Error al borrar usuario: ${err}`})

        usuario.remove(err => {
            if (!usuario) res.status(404).send({message: `Usuario no encontrado: ${err}`})
            res.status(200).send({message: 'El usuario ha sido eliminado'})
        })
    })
})

app.get('/prueba', (req, res)=>{
    
})



module.exports = app